<?php
include 'db.php';
$con = new Connection();
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$salespersonid	= $_POST['userid'];
$store_name		= fnEncodeString($_POST['store_name']);
$owner_name		= fnEncodeString($_POST['owner_name']);
$contact_no		= $_POST['contact_no'];
$store_Address	= fnEncodeString($_POST['store_Address']);
$lat			= $_POST['lat'];
$lng			= $_POST['lon'];
$city			= $_POST['city'];
$state			= $_POST['state'];
$suburbid		= $_POST['suburbid'];

$shop_type_id	= $_POST['shop_type_id'];

$bank_acc_name	= $_POST['bank_acc_name'];
$bank_acc_no	= $_POST['bank_acc_no'];
$bank_b_name	= $_POST['bank_b_name'];
$bank_ifsc		= $_POST['bank_ifsc'];

$jsonOutput 	= $JSONVar->fnAddNewStore($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$suburbid,$shop_type_id, $bank_acc_name,$bank_acc_no,$bank_b_name,$bank_ifsc);
echo $jsonOutput;
