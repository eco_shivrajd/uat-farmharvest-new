<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getOrders($order_status);//print_R($orders);
$order_count = count($orders);
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
<thead>
	<tr>
		<?php 
		$no_check_box = array(2,6);
		$delivered = 0;
		if($_SESSION[SESSION_PREFIX.'user_type']=="Admin" && $order_status == 4) {//delivered orders
			$delivered = 1;
		}
		if(!in_array($order_status,$no_check_box) && $order_count != 0 && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
		<th id="select_th">
			<input type="checkbox" name="select_all[]" id="select_all" onchange="javascript:checktotalchecked(this)">
		</th>
		<? } ?>
		<th>
			Region
		</th>
		<th>
			Regional Head
		</th>
		<th>
			Shop Name
		</th>
		<th>
			Order Id
		</th>								
		<th>
			Order Date 
		</th>	
		 <th>
			Category
		</th>	
		<th>
			Product
		</th>	
		<th>
			Quantity
		</th>
		<th>
			Price (₹)
		</th>
		<th>
			GST Price (₹)
		</th>
		<th>
			Action  
		</th>
	</tr>
</thead>
<tbody>
	<?php 
	foreach($orders as $val_order){
		?>
	<tr class="odd gradeX">
		<?php 
		
		if(!in_array($order_status,$no_check_box)  && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
		<td>
			<input type="checkbox" disabled name="select_all[]" id="select_all" value="ordermain_<?=$val_order['id'];?>" onchange="javascript:checktotalchecked(this)">
		</td>
		<?php } ?>
		<td>
			 <?=$val_order['order_by_name'];//region?>
		</td>
		<td>
			 <?=$val_order['order_by_name'];?>
		</td>
		<td>
			 <?=$val_order['shop_name'];?>
		</td>
		<td colspan="7">
		</td>
		<td style="display: none;"><!--This display: none code is to remove the datatable
		error, datatable doesnot allow to use colspan-->
		</td>
		<td style="display: none;">
		</td>
		<td style="display: none;">
		</td>
		<td style="display: none;">
		</td>
		<td style="display: none;">
		</td>
		<td style="display: none;">
		</td>
		<td>									
			<a onclick="showInvoice(<?=$order_status;?>,<?=$val_order['id'];?>)" title="View Invoice">view</a> 
		</td>								
	</tr>
	<?php 
			if(count($val_order['order_details']) > 0){
				foreach($val_order['order_details'] as $val){
				?>
				<tr class="odd gradeX">
					<?php if(!in_array($order_status,$no_check_box)  && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
					<td>
						<input type="checkbox" name="select_all[]" id="select_all" value="ordersub_<?=$val_order['id'];?>_<?=$val['id'];?>"  onchange="javascript:checktotalchecked(this)">
					</td>
					<?php } ?>
					<td colspan="3">
					</td>
					<td style="display: none;">
					</td>
					<td style="display: none;">
					</td>
					<td style="display: none;">
					</td>
					<td>
						<?=$val_order['order_no'];?>
					</td> 
					<td>
						<?=$val_order['order_date'];?>
					</td>
					<td>
						 <?=$val['cat_name'];?>
					</td>
					<td>
						 <?=$val['product_name'];?>
					</td>
					<td align="right">
						 <?=$val['product_quantity'];?>
					</td>
					<td align="right">
						<?=$val['product_total_cost'];?>
					</td>
					<td align="right">
						<?=$val['p_cost_cgst_sgst'];?>
					</td>
					<td>									
						<a onclick="showOrderDetails(<?=$val['id'];?>,'Order Details')" title="Order Details">Details</a>
					</td>									
				</tr>
				
			<?php 
				}
			}						
	} ?>
	
</tbody>
</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>