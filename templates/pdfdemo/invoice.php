<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";

?>

	<style>
		@CHARSET "UTF-8";
		.page-break {
			page-break-after: always;
			page-break-inside: avoid;
			clear:both;
		}
		.page-break-before {
			page-break-before: always;
			page-break-inside: avoid;
			clear:both;
		}
	</style>
<style>
.darkgreen{
	background-color:#364622; color:#fff; font-size:18px;font-weight:600;
}
.fentgreen{
	background-color:#b0b29c;
}
.font-big{
	font-size:16px;
	font-weight:600;
	color:#364622;
}
.table-bordered {
    border: 1px solid #364622;
}
.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border: 1px solid #364622;
}

.blue1{
	color:#574960;
	font-size:16px;
}
.pad-5{
	padding-left:10px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg{
	background-image:url(../../assets/admin/layout/img/pdf-img/watermark.png); background-repeat:no-repeat;
}

</style>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageInvoice"; $activeMenu = "Invoice";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->
			<h3 class="page-title">Brands</h3>
			
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i><a href="#">Brands</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Brands Listing
							</div>
                            <a class="btn btn-sm btn-default pull-right mt5" href="brands-add.php">
                                Add brand
                            </a>
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
						
					 <div id="content">	
						
							  <table class="table table-bordered">
                        <tbody>
                        <tr>
                        <td colspan="4" class="darkgreen"><img src="../../assets/admin/layout/img/logo-fh-invoice.jpg" style="width:60px;"> &nbsp; Sri Jaya Shree Food Product</td>
                        <td colspan="3" class="font-big text-center">Tax Invoice</td>
                        </tr>
                        
                        <tr>
                        <td colspan="4" class="fentgreen1">Plot No. 31, SIDCO Women Industrial Estate, Government Engineering COllege Post, Karuppur, Salem-636 011.<br/>
Tel: <b>+91-427-3253737</b> www.farmnarvest.in Tollfree : <b>18001232676</b> State: <b>Tamil Nadu</b> State Code: <b>33</b> GSTIN :<b>33AGIPS3575Q1ZLM</b></td>
                        <td colspan="3" rowspan="2">
                        <div class="col-md-7 np">Invoice No.: </div><span class="blue">SJFP/17-18/3161</span><br/>
                        <div class="col-md-7 np">Dated: </div><span class="blue">19-Jan-2018</span><br/>
                        <div class="col-md-7 np">D.C. No.: </div><br/>
                        <div class="col-md-7 np">Vehicle No.:</div><br/>
                        <div class="col-md-7 np">Transportation Mode:</div> <br/>
                        <div class="col-md-7 np">Date& Time of Supply:</div><br/>
                        <div class="col-md-7 np">Place of Supply:</div>
                        </td>
                        
                        </tr>
                        
                        <tr>
                        <td colspan="4">Buyer <span class="blue1"><b>Rani Food Product</b><br/>
                        No 1/2 Yercaud Main Road,<br/>
                        Chinnakollapatti, Kannaukurchi (Po)<br/>
                        Salem - 636008<br/>
                        GSTIN NO. 33BMZPS0384K1Z9</span>
                        </td>
                       
                        
                        </tr>
                        
                       <!-- <tr>
                      
                        <td>D.C. No.</td>
                        <td>:</td>
                        <td></td>
                        </tr>
                        
                        <tr>
                      
                        <td>Vehicle No.</td>
                        <td>:</td>
                        <td></td>
                        </tr>
                        
                        <tr>
                       
                        <td>Transportation Mode</td>
                        <td>:</td>
                        <td></td>
                        </tr>
                        
                        <tr>
                        
                        <td>Date& Time of Supply</td>
                        <td>:</td>
                        <td></td>
                        </tr>
                        
                        <tr>
                        
                        <td>Place of Supply</td>
                        <td>:</td>
                        <td></td>
                        </tr>-->
                        
                        <tr class="fentgreen">
                        <th width="5%" class="text-center">SL No.</th>
                        <th class="text-center">Name of Goods</th>
                        <th class="text-center">HSN COde</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Rate</th>
                        <th class="text-center">UOM</th>
                        <th class="text-center">Value</th>
                        </tr>
                        
                        <tr style="height:214px;">
                        <td class="text-center"><span class="blue">1</span></td>
                        <td class="bg text-center"><span class="blue">1.350kgs Sweet Corn Kernels Packed</span></td>
                        <td class="text-center"><span class="blue">07104000</span></td>
                        <td class="text-center"><span class="blue">32</span></td>
                        <td class="text-center"><span class="blue">72.38</span></td>
                        <td class="text-center"><span class="blue">nos</span></td>
                        <td class="text-center"><span class="blue">2.316.16</span></td>
                        </tr>
                        
                        
                        <tr>
                        <td></td>
                        <td class="text-right"><b>Total</b></td>
                        <td class="fentgreen"></td>
                        <td class="fentgreen">32</td>
                        <td class="fentgreen"></td>
                        <td class="fentgreen"></td>
                        <td class="fentgreen">2.316.16</td>
                        </tr>
                        
                        <tr>
                        <td colspan="4"><b>Two Thousand Four Hunderd Thirty Two only</b>
                        
                        <table class="table table-bordered">
                        <tbody>
                        <tr>
                        <td class="text-center"><span class="blue">HSN/SAC</span></td>
                        <td class="text-center"><span class="blue">Taxable Value</span></td>
                        <td colspan="2" class="text-center"><span class="blue">Central Tax</span> </td>
                        <td class="text-center"><span class="blue">Rate</span></td>
                        <td colspan="1" class="text-center"><span class="blue">State Tax</span></td>
                        </tr>
                        
                        <tr>
                        <td></td>
                        <td></td>
                        <td><span class="blue">Rate</span> </td>
                        <td><span class="blue">Amount</span></td>
                        <td></td>
                        <td><span class="blue">Amount</span></td>
                        </tr>
                        
                        <tr>
                        <td><span class="blue">07104000</span></td>
                        <td> <span class="blue">2,316.16</span></td>
                        <td><span class="blue"> 2.50%</span></td>
                        <td><span class="blue"> 57.90</span></td>
                        <td><span class="blue"> 2.50%</span></td>
                        <td><span class="blue"> 57.90</span></td>
                        </tr>
                        
                        <tr>
                        <td class="text-right"><span class="blue">Total</span></td>
                        <td><span class="blue">2,316.16</span></td>
                        <td></td>
                        <td><span class="blue">57.90</span></td>
                        <td> </td>
                        <td><span class="blue">57.90</span></td>
                        </tr>
                        
                        </tbody>
                        </table>
                        
                        </td>
                        <td colspan="2" rowspan="2"><span class="blue">CGST@2.5%</span><br/>
                        <span class="blue">SGST@2.5%</span><br/>
                        <span class="blue">Rounding Off</span> <br/>
                        <span class="blue">Grand Total </span><br/>
                        <span class="blue">Opening Balance</span> <br/>
                        <span class="blue">Closing Balance </span>
                        </td>
                        <td rowspan="2">57.90</span><br/>
                        <span class="blue">57.90</span><br/>
                        <span class="blue">0.04</span><br/>
                        <span class="blue">2,432.00</span><br/>
                        <span class="blue">18,03,202.00</span><br/>
                        <span class="blue">18,05,634.00</span>
                        </td>
                        </tr>
                        
                        <tr>
                        <td colspan="2">
                        <u>Declaration:</u><br/>
                        <ol class="pad-5">
<li>I/We declare that this invoice shows actual proce of good and / or services described and<br/> that all particulars are true and correct</li>
<li>Error & Omission Expected</li>
<li>Subjects to the jurisdiction of courts in Salem</li>
</ol>
                        </td>
                        <td><div class="text-center"><b><u>BANK DETAILS</u></b></div>
                        <div class="col-md-5 np">BANK NAME:</div> HDFC BANK,<br/>
<div class="col-md-5 np">BRANCH:</div> SALEM MAIN, FIVE ROADS<br/>
<div class="col-md-5 np">CC A/C NO.:</div> 50200013167362<br/>
<div class="col-md-5 np">IFSC CODE:</div> HDFC0000178</td>
                        <td  class="fentgreen font-big1">For <b>SRI JAYA SHREE FOOD PRODUCTIONS</b><br/><br/><br/>
                        Authorised Signature
                        </td>
                      
                        </tr>
                        
                     
                        
                        </tbody>
                        
                        </table>

</div>
<button id="cmd">Generate PDF</button>



						</div>
					</div>                  
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->



<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>


<script>
var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};
$('#cmd').click(function () {   
    doc.fromHTML($('#content').html(), 15, 15, {
        'width': 170,
        'color':'#574960',
//'border': 1px solid #364622,
            'elementHandlers': specialElementHandlers
    });
$('#content').css('<link rel="stylesheet" href="style.css" type="text/css" />');

// $('#test').css("border: 1px solid #364622;");
//$('.blue').css("color:#574960;");
//$('.blue1').css("color:#574960;");
//$('.blue1').css("font-size:16px;");

$('#test').html('<link rel="stylesheet" href="style.css" type="text/css" />'); 
    doc.save('sample-file.pdf');
}); 
</script>




</body>
<!-- END BODY -->
</html>